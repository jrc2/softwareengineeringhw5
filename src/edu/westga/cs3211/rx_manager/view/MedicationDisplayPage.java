package edu.westga.cs3211.rx_manager.view;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import edu.westga.cs3211.rx_manager.model.AmPm;
import edu.westga.cs3211.rx_manager.model.DayTimePair;
import edu.westga.cs3211.rx_manager.model.FoodRequirements;
import edu.westga.cs3211.rx_manager.model.RX;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;

/**
 * Code behind for the Add Page.
 * 
 * @author John Chittam
 * @version HW5
 */
public class MedicationDisplayPage {
	@FXML private ResourceBundle resources;
	@FXML private URL location;

	@FXML private TextField name;
	@FXML private TextField dosage;
	@FXML private TextField refillsPerRenew;
	@FXML private TextField dosesPerRefill;
	@FXML private TextField hour;
	@FXML private TextField minutes;
	@FXML private TextArea stepsToTake;
	@FXML private ComboBox<AmPm> amPm;
	@FXML private ComboBox<DayOfWeek> dayOfWeek;
	@FXML private ComboBox<FoodRequirements> takeWithFoodSelection;
	@FXML private ListView<DayTimePair> dayTimePairs;
	@FXML private Button addDayTimePair;
	@FXML private Button editDayTimePair;
	@FXML private Button removeDayTimePair;
	@FXML private Button addMedication;
	@FXML private Button cancel;
	@FXML private Button viewRecord;
	@FXML private Button saveDetails;
	@FXML private CheckBox noDriving;
	@FXML private CheckBox noAlcohol;
	@FXML private Label timeSeparator;
	
	private ObservableList<RX> rxs;
	private int selectedIndex;

	/**
	 * Binds the data from the RX object to the appropriate display fields.
	 * 
	 * @param rxs the list of RX objects to add to
	 * 
	 * @return if the binding was successful
	 */
	public boolean bind(ObservableList<RX> rxs) {
		if (rxs == null) {
			return false;
		}
		this.rxs = rxs;
		return true;
	}
	
	/**
	 * Shows the details for the given RX, but does not allow editing.
	 *
	 * @param rx the rx to show details for
	 */
	public void changeToDetailsView(RX rx) {
		this.hideBottomButtons();
		this.viewRecord.setVisible(true);
		this.fillValuesFromRx(rx);
		this.disableEntryPoints();
		this.hideTimeSettings();
	}
	
	private void fillValuesFromRx(RX rx) {
		this.name.setText(rx.getName());
		this.dosage.setText(rx.getDosage());
		this.refillsPerRenew.setText(rx.getRefillsPerRenew() + "");
		this.dosesPerRefill.setText(rx.getDosesPerRefill() + "");
		
		this.stepsToTake.setText(rx.getStepsToTake());
		this.noDriving.setSelected(rx.noDrivingAfterTaking());
		this.noAlcohol.setSelected(rx.noAlcoholAfterTaking());
		
		for (DayTimePair dayTimePair : rx.getDayTimePairs()) {
			this.dayTimePairs.getItems().add(dayTimePair);
		}
		
		this.takeWithFoodSelection.getSelectionModel().select(rx.getFoodRequirements());
	}
	
	private void disableEntryPoints() {
		this.name.setDisable(true);
		this.dosage.setDisable(true);
		this.refillsPerRenew.setDisable(true);
		this.dosesPerRefill.setDisable(true);
		this.stepsToTake.setDisable(true);
		this.noDriving.setDisable(true);
		this.noAlcohol.setDisable(true);
		this.takeWithFoodSelection.setDisable(true);
	}
	
	private void hideTimeSettings() {
		this.hour.setVisible(false);
		this.timeSeparator.setVisible(false);
		this.minutes.setVisible(false);
		this.amPm.setVisible(false);
		this.dayOfWeek.setVisible(false);
		this.addDayTimePair.setVisible(false);
		this.editDayTimePair.setVisible(false);
		this.removeDayTimePair.setVisible(false);
	}

	private void hideBottomButtons() {
		this.addMedication.setVisible(false);
		this.cancel.setVisible(false);
		this.viewRecord.setVisible(false);
		this.saveDetails.setVisible(false);
	}

	@FXML
	void addMedication(ActionEvent event) {
		try {
			RX rx = new RX(this.name.getText(), this.dosage.getText(),
					Integer.parseInt(this.refillsPerRenew.getText()), Integer.parseInt(this.dosesPerRefill.getText()),
					this.stepsToTake.getText(), this.noDriving.isSelected(), this.noAlcohol.isSelected(), this.dayTimePairs.getItems(), this.takeWithFoodSelection.getValue());
			this.rxs.add(rx);
			((Node) (event.getSource())).getScene().getWindow().hide();
			new Alert(AlertType.INFORMATION, "Medication added successfully.").showAndWait();
		} catch (Exception e) {
			if (e.getClass() != NumberFormatException.class) {
				new Alert(AlertType.ERROR, e.getMessage() + " -  Please correct information and resubmit.").showAndWait();
			} else {
				new Alert(AlertType.ERROR, "Refills per renew and doses per refill must be a number.  Please correct information and resubmit.").showAndWait();
			}
		}

	}
	
	/**
	 * Shows the details for the given RX and allows editing/
	 *
	 * @param rx the rx to show details for
	 */
	public void changeToEditView(RX rx) {
		this.hideBottomButtons();
		this.saveDetails.setVisible(true);
		this.cancel.setVisible(true);
		this.fillValuesFromRx(rx);
		this.selectedIndex = this.rxs.indexOf(rx);
	}
	
	@FXML
	void saveDetails(ActionEvent event) {
		try {
			RX rx = this.rxs.get(this.selectedIndex);
			rx.setName(this.name.getText());
			rx.setDosage(this.dosage.getText());
			rx.setRefillsPerRenew(Integer.parseInt(this.refillsPerRenew.getText()));
			rx.setDosesPerRefill(Integer.parseInt(this.dosesPerRefill.getText()));
			rx.setStepsToTake(this.stepsToTake.getText());
			rx.setNoDrivingAfterTaking(this.noDriving.isSelected());
			rx.setNoAlcoholAfterTaking(this.noAlcohol.isSelected());
			rx.setDayTimePairs(this.dayTimePairs.getItems());
			rx.setFoodRequirements(this.takeWithFoodSelection.getValue());
			this.rxs.set(this.selectedIndex, rx);
			((Node) (event.getSource())).getScene().getWindow().hide();
			new Alert(AlertType.INFORMATION, "Medication updated successfully.").showAndWait();
		} catch (Exception e) {
			if (e.getClass() != NumberFormatException.class) {
				new Alert(AlertType.ERROR, e.getMessage() + " -  Please correct information and resubmit.").showAndWait();
			} else {
				new Alert(AlertType.ERROR, "Refills per renew and doses per refill must be a number. Please correct information and resubmit.").showAndWait();
			}
		}
		
		
	}
	
	@FXML
	void addDayTimePair(ActionEvent event) {
		try {
			DayTimePair dayTimePair = this.createDayTimePair();
			if (!this.dayTimePairs.getItems().contains(dayTimePair)) {
				this.dayTimePairs.getItems().add(dayTimePair);
			} else {
				new Alert(AlertType.ERROR, "Day/Time pair already exists.  Please correct information and resubmit.").showAndWait();
			}
		} catch (Exception e) {
			if (e.getClass() != NumberFormatException.class) {
				new Alert(AlertType.ERROR, e.getMessage()).showAndWait();
			} else {
				new Alert(AlertType.ERROR, "Hour and minutes must be numbers.  Please correct information and resubmit.").showAndWait();
			}
			
		}
		
	}

	@FXML
	void editDayTimePair(ActionEvent event) {
		DayTimePair dayTimePair = this.createDayTimePair();
		if (this.dayTimePairs.getSelectionModel().isEmpty()) {
			new Alert(AlertType.ERROR, "You must select a day/time pair to edit").showAndWait();
			return;
		}
		int index = this.dayTimePairs.getSelectionModel().getSelectedIndex();
		this.removeDayTimePair(null);
		this.dayTimePairs.getItems().add(index, dayTimePair);
		this.dayTimePairs.getSelectionModel().select(index);
	}
	
	private DayTimePair createDayTimePair() {
		return new DayTimePair(this.dayOfWeek.getValue(), this.hour.getText(), this.minutes.getText(), this.amPm.getValue());
	}
	
	@FXML
	void removeDayTimePair(ActionEvent event) {
		if (this.dayTimePairs.getSelectionModel().isEmpty()) {
			new Alert(AlertType.ERROR, "You must select a day/time pair to remove").showAndWait();
			return;
		}
		int index = this.dayTimePairs.getSelectionModel().getSelectedIndex();
		this.dayTimePairs.getItems().remove(index);
	}
	
	@FXML
	void dayTimePairSelected(MouseEvent event) {
		MultipleSelectionModel<DayTimePair> selectionModel = this.dayTimePairs.getSelectionModel();
		DayTimePair selectedDayTime = selectionModel.getSelectedItem();
		this.hour.setText(selectedDayTime.getTime().format(DateTimeFormatter.ofPattern("h", Locale.US)));
		this.minutes.setText(selectedDayTime.getTime().format(DateTimeFormatter.ofPattern("mm", Locale.US)));
		AmPm amPm = AmPm.valueOf(selectedDayTime.getTime().format(DateTimeFormatter.ofPattern("a", Locale.US)));
		this.amPm.getSelectionModel().select(amPm);
		this.dayOfWeek.getSelectionModel().select(selectedDayTime.getDay());
		this.editDayTimePair.setDisable(false);
		this.removeDayTimePair.setDisable(false);
	}

	@FXML
	void cancel(ActionEvent event) {
		((Node) (event.getSource())).getScene().getWindow().hide();

	}

	@FXML
	void initialize() {
		this.initilizeAssertions();
		this.initializeAmPmComboBox();
		this.initializeFoodRequirementsComboBox();
		this.initializeDayOfTheWeekComboBox();
	}

	private void initilizeAssertions() {
		assert this.name != null : "fx:id=\"name\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.dosage != null : "fx:id=\"dosage\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.refillsPerRenew != null : "fx:id=\"refillsPerRenew\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.dosesPerRefill != null : "fx:id=\"dosesPerRefill\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.hour != null : "fx:id=\"hour\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.minutes != null : "fx:id=\"minutes\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.stepsToTake != null : "fx:id=\"stepsToTake\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.amPm != null : "fx:id=\"amPm\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.dayOfWeek != null : "fx:id=\"dayOfWeek\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.takeWithFoodSelection != null : "fx:id=\"takeWithFoodSelection\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.dayTimePairs != null : "fx:id=\"dayTimePairs\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.addDayTimePair != null : "fx:id=\"addDayTimePair\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.editDayTimePair != null : "fx:id=\"editDayTimePair\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.removeDayTimePair != null : "fx:id=\"removeDayTimePair\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.addMedication != null : "fx:id=\"addMedication\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.cancel != null : "fx:id=\"cancel\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.viewRecord != null : "fx:id=\"viewRecord\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.saveDetails != null : "fx:id=\"saveDetails\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.noDriving != null : "fx:id=\"noDriving\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.noAlcohol != null : "fx:id=\"noAlcohol\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
		assert this.timeSeparator != null : "fx:id=\"timeSeparator\" was not injected: check your FXML file 'MedicationDisplayPage.fxml'.";
	}
	
	private void initializeAmPmComboBox() {
		for (AmPm amPmOption : AmPm.values()) {
			this.amPm.getItems().add(amPmOption);
		}
		
		this.amPm.getSelectionModel().selectFirst();
	}

	private void initializeFoodRequirementsComboBox() {
		for (FoodRequirements requirement : FoodRequirements.values()) {
			this.takeWithFoodSelection.getItems().add(requirement);
		}
		
		this.takeWithFoodSelection.getSelectionModel().selectFirst();
	}
	
	private void initializeDayOfTheWeekComboBox() {
		for (DayOfWeek day : DayOfWeek.values()) {
			this.dayOfWeek.getItems().add(day);
		}
		
		this.dayOfWeek.getSelectionModel().selectFirst();
	}
}
