package edu.westga.cs3211.rx_manager.view;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import edu.westga.cs3211.rx_manager.Main;
import edu.westga.cs3211.rx_manager.model.RX;

/** Code behind for the Landing Page.
 * 
 * @author John Chittam
 * @version HW5
 */
public class LandingPage {
    @FXML private ResourceBundle resources;
    @FXML private URL location;
    @FXML private ListView<RX> prescriptions;

    @FXML
    void removePrescription(ActionEvent event) {
    	final RX selectedItem = this.prescriptions.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			Alert removalConfirmation = new Alert(AlertType.CONFIRMATION);
			removalConfirmation.setHeaderText("Do you really want to delete selected medication?");
			removalConfirmation.setContentText("That cannot be undone.");
			Optional<ButtonType> result = removalConfirmation.showAndWait();
			if (result.get() == ButtonType.OK) {
				this.prescriptions.getItems().remove(selectedItem);
			}
    		
    	} else {
    		new Alert(AlertType.ERROR, "You must select a medication to remove").showAndWait();
    	}
    }
    
    @FXML
    void addPrescription(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(Main.class.getResource(Main.MEDICATION_DISPLAY_PAGE));
    	loader.load();
    	Parent parent = loader.getRoot();
    	Scene scene = new Scene(parent);
    	Stage addTodoStage = new Stage();
    	addTodoStage.setTitle(Main.TITLE);
    	addTodoStage.setScene(scene);
    	addTodoStage.initModality(Modality.APPLICATION_MODAL);
    	MedicationDisplayPage addPage = loader.getController();
    	addPage.bind(this.prescriptions.getItems());
    	
    	addTodoStage.show();

    }

    @FXML
    void displayPrescriptionDetails(Event event) throws IOException {
    	final RX selectedItem = this.prescriptions.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			FXMLLoader loader = new FXMLLoader();
	    	loader.setLocation(Main.class.getResource(Main.MEDICATION_DISPLAY_PAGE));
	    	loader.load();
	    	Parent parent = loader.getRoot();
	    	Scene scene = new Scene(parent);
	    	Stage addTodoStage = new Stage();
	    	addTodoStage.setTitle(Main.TITLE);
	    	addTodoStage.setScene(scene);
	    	addTodoStage.initModality(Modality.APPLICATION_MODAL);
	    	MedicationDisplayPage addPage = loader.getController();
	    	addPage.bind(this.prescriptions.getItems());
	    	addPage.changeToDetailsView(selectedItem);
	    	
	    	addTodoStage.show();
    	} else {
    		new Alert(AlertType.ERROR, "You must select a medication to display details").showAndWait();
    	}
    }
    
    @FXML
    void displayRxEditPage(ActionEvent event) throws IOException {
    	final RX selectedItem = this.prescriptions.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			FXMLLoader loader = new FXMLLoader();
	    	loader.setLocation(Main.class.getResource(Main.MEDICATION_DISPLAY_PAGE));
	    	loader.load();
	    	Parent parent = loader.getRoot();
	    	Scene scene = new Scene(parent);
	    	Stage addTodoStage = new Stage();
	    	addTodoStage.setTitle(Main.TITLE);
	    	addTodoStage.setScene(scene);
	    	addTodoStage.initModality(Modality.APPLICATION_MODAL);
	    	MedicationDisplayPage addPage = loader.getController();
	    	addPage.bind(this.prescriptions.getItems());
	    	addPage.changeToEditView(selectedItem);
	    	
	    	addTodoStage.show();
    	} else {
    		new Alert(AlertType.ERROR, "You must select a medication to edit").showAndWait();
    	}
    }

    @FXML
    void initialize() {
        assert this.prescriptions != null : "fx:id=\"prescriptions\" was not injected: check your FXML file 'LandingPage.fxml'.";
        
    }
}
