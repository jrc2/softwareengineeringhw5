package edu.westga.cs3211.rx_manager.model;

import javafx.collections.ObservableList;

/** Stores and manages information for a single Prescription.
 * 
 * @author John Chittam
 * @version HW5
 */
public class RX {
	private String name;
	private String dosage;
	private int refillsPerRenew;
	private int dosesPerRefill;
	private String stepsToTake;
	private boolean noDrivingAfterTaking;
	private boolean noAlcoholAfterTaking;
	private ObservableList<DayTimePair> dayTimePairs;
	private FoodRequirements foodRequirements;
	
	private static final String BLANK_NAME = "name must be filled in.";
	private static final String BLANK_DOSAGE = "dosage must be filled in.";
	private static final String BLANK_STEPS = "steps to take must be filled in.";
	private static final String NEGATIVE_REFILLS = "Refills per renew cannot be negative.";
	private static final String LESS_THAN_ONE_DOSE = "There must be at least one dose per refill.";
	private static final String EMPTY_DAY_TIME_PAIR = "You must add at least one day/time pair to schedule.";
	
	/**
	 * Create a new Prescription with the provided information.
	 *
	 * @param name the Prescription name
	 * @param dosage the Prescription dosage
	 * @param refillsPerRenew the number of refills per renew
	 * @param dosesPerRefill the number of doses per refill
	 * @param stepsToTake the steps to take when taking medication
	 * @param noDrivingAfterTaking true if you cannot drive when taking medication
	 * @param noAlcoholAfterTaking true if you cannot drink alcohol when taking medication
	 * @param dayTimePairs the day of the week and time pair for medication schedule
	 * @param foodRequirements the food requirements of the medication
	 */
	public RX(String name, String dosage, int refillsPerRenew, int dosesPerRefill, String stepsToTake, boolean noDrivingAfterTaking, boolean noAlcoholAfterTaking, 
			ObservableList<DayTimePair> dayTimePairs, FoodRequirements foodRequirements) {
		if (name.isBlank()) {
			throw new IllegalArgumentException(BLANK_NAME);
		}
		if (dosage.isBlank()) {
			throw new IllegalArgumentException(BLANK_DOSAGE);
		}
		if (stepsToTake.isBlank()) {
			throw new IllegalArgumentException(BLANK_STEPS);
		}
		if (refillsPerRenew < 0) {
			throw new IllegalArgumentException(NEGATIVE_REFILLS);
		}
		if (dosesPerRefill < 1) {
			throw new IllegalArgumentException(LESS_THAN_ONE_DOSE);
		}
		if (dayTimePairs.isEmpty()) {
			throw new IllegalArgumentException(EMPTY_DAY_TIME_PAIR);
		}
		this.name = name;
		this.dosage = dosage;
		this.refillsPerRenew = refillsPerRenew;
		this.dosesPerRefill = dosesPerRefill;
		this.stepsToTake = stepsToTake;
		this.noAlcoholAfterTaking = noDrivingAfterTaking;
		this.noAlcoholAfterTaking = noAlcoholAfterTaking;
		this.dayTimePairs = dayTimePairs;
		this.foodRequirements = foodRequirements;
	}
	
	/** 
	 * Gets the name of the RX
	 * 
	 * @return the name of the RX
	 */
	public String getName() {
		return this.name;
	}

	/** 
	 * Gets the dosage amount for the RX
	 * 
	 * @return the dosage amount for the RX
	 */
	public String getDosage() {
		return this.dosage;
	}

	/** 
	 * Gets the number of refills per prescription renewal
	 * 
	 * @return the number of refills per prescription renewal
	 */
	public int getRefillsPerRenew() {
		return this.refillsPerRenew;
	}

	/** 
	 * Gets the number of doses per refill
	 * 
	 * @return the number of doses per refill
	 */
	public int getDosesPerRefill() {
		return this.dosesPerRefill;
	}

	/** 
	 * Gets what to do when taking the RX
	 * 
	 * @return what to do when taking the RX
	 */
	public String getStepsToTake() {
		return this.stepsToTake;
	}

	/** 
	 * Can I not drive while taking this medication?
	 * 
	 * @return true if you cannot drive while taking, false if you can
	 */
	public boolean noDrivingAfterTaking() {
		return this.noDrivingAfterTaking;
	}
	
	/** 
	 * Can I not drink alcohol while taking this medication?
	 * 
	 * @return true if you cannot drink alcohol while taking, false if you can
	 */
	public boolean noAlcoholAfterTaking() {
		return this.noAlcoholAfterTaking;
	}

	/**
	 * Gets the day time pairs.
	 *
	 * @return the day time pairs
	 */
	public ObservableList<DayTimePair> getDayTimePairs() {
		return this.dayTimePairs;
	}
	
	/**
	 * Gets the food requirements.
	 *
	 * @return the food requirements
	 */
	public FoodRequirements getFoodRequirements() {
		return this.foodRequirements;
	}
	

	/**
	 * Sets the name
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name.isBlank()) {
			throw new IllegalArgumentException(BLANK_NAME);
		}
		
		this.name = name;
	}

	/**
	 * Sets the dosage 
	 * 
	 * @param dosage the dosage to set
	 */
	public void setDosage(String dosage) {
		if (dosage.isBlank()) {
			throw new IllegalArgumentException(BLANK_DOSAGE);
		}
		
		this.dosage = dosage;
	}

	/**
	 * Sets the number of refills per renew
	 * 
	 * @param refillsPerRenew the refillsPerRenew to set
	 */
	public void setRefillsPerRenew(int refillsPerRenew) {
		if (refillsPerRenew < 0) {
			throw new IllegalArgumentException(NEGATIVE_REFILLS);
		}
		
		this.refillsPerRenew = refillsPerRenew;
	}

	/**
	 * Sets the number of doses per refill
	 * 
	 * @param dosesPerRefill the dosesPerRefill to set
	 */
	public void setDosesPerRefill(int dosesPerRefill) {
		if (dosesPerRefill < 1) {
			throw new IllegalArgumentException(LESS_THAN_ONE_DOSE);
		}
		
		this.dosesPerRefill = dosesPerRefill;
	}

	/**
	 * Sets the written steps to take
	 * 
	 * @param stepsToTake the stepsToTake to set
	 */
	public void setStepsToTake(String stepsToTake) {
		if (stepsToTake.isBlank()) {
			throw new IllegalArgumentException(BLANK_STEPS);
		}
		
		this.stepsToTake = stepsToTake;
	}

	/**
	 * Sets whether or not driving is allowed after taking
	 * 
	 * @param noDrivingAfterTaking the noDrivingAfterTaking to set
	 */
	public void setNoDrivingAfterTaking(boolean noDrivingAfterTaking) {
		this.noDrivingAfterTaking = noDrivingAfterTaking;
	}

	/**
	 * Sets whether or not alcohol can be consumed after taking
	 * 
	 * @param noAlcoholAfterTaking the noAlcoholAfterTaking to set
	 */
	public void setNoAlcoholAfterTaking(boolean noAlcoholAfterTaking) {
		this.noAlcoholAfterTaking = noAlcoholAfterTaking;
	}

	/**
	 * Sets the day time pair for th rx
	 * 
	 * @param dayTimePairs the dayTimePairs to set
	 */
	public void setDayTimePairs(ObservableList<DayTimePair> dayTimePairs) {
		if (dayTimePairs.isEmpty()) {
			throw new IllegalArgumentException(EMPTY_DAY_TIME_PAIR);
		}
		
		this.dayTimePairs = dayTimePairs;
	}

	/**
	 * Sets the food requirements
	 * 
	 * @param foodRequirements the foodRequirements to set
	 */
	public void setFoodRequirements(FoodRequirements foodRequirements) {
		this.foodRequirements = foodRequirements;
	}

	/** 
	 * Convert object to a String representation.
	 * 
	 * @return String representation of the RX object
	 */
	@Override
	public String toString() {		
		return this.name + ": " + this.dosage;
	}

	
}
