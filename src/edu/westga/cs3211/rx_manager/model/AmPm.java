package edu.westga.cs3211.rx_manager.model;

/** AM or PM for taking a medication.
 * 
 * @author John Chittam
 * @version HW5
 */
public enum AmPm {
	AM, PM; 
}
