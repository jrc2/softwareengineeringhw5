package edu.westga.cs3211.rx_manager.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

/**
 * Stores the day and time taken for a medicine.
 *
 * @author John Chittam
 */
public class DayTimePair {
	
	private DayOfWeek day;
	
	private LocalTime time;
	
	private static DateTimeFormatter timeFormatterSingleDigitMinutes = DateTimeFormatter.ofPattern("h:m a", Locale.US);
	private static DateTimeFormatter timeFormatterDoubleDigitMinutes = DateTimeFormatter.ofPattern("h:mm a", Locale.US);
	
	/**
	 * Instantiates a new day time pair, which contains a day and time to add to a medication schedule.
	 *
	 * @param day the day of the week
	 * @param time the time
	 */
	public DayTimePair(DayOfWeek day, LocalTime time) {
		this.day = day;
		this.time = time;
	}
	
	/**
	 * Instantiates a new day time pair using separate hour, minutes, and am or pm
	 *
	 * @param day the day of the week
	 * @param hour the hour portion of the time, assuming 12-hour time
	 * @param minutes the minutes portion of the time
	 * @param amPm the am or pm choice for the time
	 */
	public DayTimePair(DayOfWeek day, String hour, String minutes, AmPm amPm) {
		this(day, null);
		if (Integer.parseInt(hour) < 1 || Integer.parseInt(hour) > 12) {
			throw new IllegalArgumentException("Hour must be between 1 and 12, inclusive");
		}
		if (Integer.parseInt(minutes) < 0 || Integer.parseInt(minutes) > 59) {
			throw new IllegalArgumentException("Minutes must be between 0 and 59, inclusive");
		}
		LocalTime time = setTimeFromFormattedValues(hour, minutes, amPm);
		this.time = time;
		
	}

	/**
	 * Gets the day of the week
	 *
	 * @return the day of the week
	 */
	public DayOfWeek getDay() {
		return this.day;
	}

	/**
	 * Sets the day of the week.
	 *
	 * @param day the new day of the week
	 */
	public void setDay(DayOfWeek day) {
		this.day = day;
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public LocalTime getTime() {
		return this.time;
	}
	
	/**
	 * Gets the time, formatted based on timeFormatterDoubleDigitMinutes
	 *
	 * @return the formatted time
	 */
	public String getFormattedTime() {
		String formattedTime = this.time.format(timeFormatterDoubleDigitMinutes);
		return formattedTime;
	}

	/**
	 * Sets the time, given a LocalTime
	 *
	 * @param time the new time
	 */
	public void setTime(LocalTime time) {
		this.time = time;
	}
	
	private static LocalTime setTimeFromFormattedValues(String hour, String minutes, AmPm amPm) {
		LocalTime time = LocalTime.parse(hour + ":" + minutes + " " + amPm.toString(), 
				timeFormatterSingleDigitMinutes);
		
		return time;
	}
	
	@Override
	public String toString() {
		return this.day.toString() + " " + this.time.format(timeFormatterDoubleDigitMinutes);
	}
	
	@Override
    public boolean equals(Object object) { 
		if (!(object instanceof DayTimePair)) {
			return false;
		}
		
		DayTimePair otherDayTimePair = (DayTimePair) object;
		if (this.day.equals(otherDayTimePair.getDay()) && this.time.equals(otherDayTimePair.getTime())) {
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.day, this.time);
	}
	
	
}
