package edu.westga.cs3211.rx_manager.model.test.RX;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.DayOfWeek;
import java.time.LocalTime;
import javax.annotation.processing.Generated;

import org.junit.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;
import edu.westga.cs3211.rx_manager.model.FoodRequirements;
import edu.westga.cs3211.rx_manager.model.RX;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@Generated(value = "org.junit-tools-1.1.0")
public class TestSetRxDetails {

	private RX createTestSubject() {
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		return new RX("name", "dosage", 2, 3, "steps", false, false, dayTimePairs, FoodRequirements.TAKE_WITH_FOOD);
	}

	RX testSubject;
	String name = "name";
	String dosage = "dosage";
	int refillsPerRenew = 2;
	int dosesPerRefill = 2;
	String stepsToTake = "steps";
	boolean noDrivingAfterTaking = false;
	boolean noAlcoholAfterTaking = false;
	FoodRequirements foodRequirements = FoodRequirements.TAKE_WITH_FOOD;

	@Test
	public void testEverythingValid() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		testSubject.setName(name);
		testSubject.setDosage(dosage);
		testSubject.setRefillsPerRenew(refillsPerRenew);
		testSubject.setDosesPerRefill(dosesPerRefill);
		testSubject.setStepsToTake(stepsToTake);
		testSubject.setNoDrivingAfterTaking(noDrivingAfterTaking);
		testSubject.setNoAlcoholAfterTaking(noAlcoholAfterTaking);
		testSubject.setDayTimePairs(dayTimePairs);
		testSubject.setFoodRequirements(foodRequirements);
		assertEquals(testSubject.getName(), name);
		assertEquals(testSubject.getDosage(), dosage);
		assertEquals(testSubject.getRefillsPerRenew(), refillsPerRenew);
		assertEquals(testSubject.getDosesPerRefill(), dosesPerRefill);
		assertEquals(testSubject.getStepsToTake(), stepsToTake);
		assertEquals(testSubject.noDrivingAfterTaking(), noDrivingAfterTaking);
		assertEquals(testSubject.noAlcoholAfterTaking(), noAlcoholAfterTaking);
		assertEquals(testSubject.getDayTimePairs(), dayTimePairs);
		assertEquals(testSubject.getFoodRequirements(), foodRequirements);
	}

	@Test
	public void testNullName() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		name = null;
		assertThrows(NullPointerException.class, () -> {
			testSubject.setName(name);
		});
	}

	@Test
	public void testNullDosage() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		dosage = null;
		assertThrows(NullPointerException.class, () -> {
			testSubject.setDosage(dosage);
		});
	}

	@Test
	public void testNullSteps() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		stepsToTake = null;
		assertThrows(NullPointerException.class, () -> {
			testSubject.setStepsToTake(stepsToTake);
		});
	}

	@Test
	public void testEmpyName() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		name = "";
		assertThrows(IllegalArgumentException.class, () -> {
			testSubject.setName(name);
		});
	}

	@Test
	public void testEmpyDosage() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		dosage = "";
		assertThrows(IllegalArgumentException.class, () -> {
			testSubject.setDosage(dosage);
		});
	}

	@Test
	public void testEmpySteps() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		stepsToTake = "";
		assertThrows(IllegalArgumentException.class, () -> {
			testSubject.setStepsToTake(stepsToTake);
		});
	}

	@Test
	public void testZeroRefills() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		refillsPerRenew = 0;
		testSubject.setRefillsPerRenew(refillsPerRenew);
	}

	@Test
	public void testNegativeRefills() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		refillsPerRenew = -1;
		assertThrows(IllegalArgumentException.class, () -> {
			testSubject.setRefillsPerRenew(refillsPerRenew);
		});
	}

	@Test
	public void testZeroDoses() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		dosesPerRefill = 0;
		assertThrows(IllegalArgumentException.class, () -> {
			testSubject.setDosesPerRefill(dosesPerRefill);
		});
	}

	@Test
	public void testEmptyDayTimePairs() {
		testSubject = createTestSubject();
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		assertThrows(IllegalArgumentException.class, () -> {
			testSubject.setDayTimePairs(dayTimePairs);
		});
	}
}