package edu.westga.cs3211.rx_manager.model.test.RX;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;
import edu.westga.cs3211.rx_manager.model.FoodRequirements;
import edu.westga.cs3211.rx_manager.model.RX;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

class TestToString {

	private RX createTestSubject() {
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		return new RX("name", "dosage", 2, 3, "steps", false, false, dayTimePairs, FoodRequirements.TAKE_WITH_FOOD);
	}

	@Test
	public void testToString() throws Exception {
		RX testSubject;
		String result;

		testSubject = createTestSubject();
		result = testSubject.toString();
		
		assertEquals("name: dosage", result);
	}

}
