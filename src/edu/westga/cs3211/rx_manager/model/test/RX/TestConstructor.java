package edu.westga.cs3211.rx_manager.model.test.RX;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.junit.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;
import edu.westga.cs3211.rx_manager.model.FoodRequirements;
import edu.westga.cs3211.rx_manager.model.RX;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TestConstructor {
	String name = "name";
	String dosage = "dosage";
	int refillsPerRenew = 2;
	int dosesPerRefill = 2;
	String stepsToTake = "steps";
	boolean noDrivingAfterTaking = false;
	boolean noAlcoholAfterTaking = false;
	FoodRequirements foodRequirements = FoodRequirements.TAKE_WITH_FOOD;

	@Test
	public void testEverythingValid() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
				noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		assertEquals(rx.getName(), name);
		assertEquals(rx.getDosage(), dosage);
		assertEquals(rx.getRefillsPerRenew(), refillsPerRenew);
		assertEquals(rx.getDosesPerRefill(), dosesPerRefill);
		assertEquals(rx.getStepsToTake(), stepsToTake);
		assertEquals(rx.noDrivingAfterTaking(), noDrivingAfterTaking);
		assertEquals(rx.noAlcoholAfterTaking(), noAlcoholAfterTaking);
		assertEquals(rx.getDayTimePairs(), dayTimePairs);
		assertEquals(rx.getFoodRequirements(), foodRequirements);
	}

	@Test
	public void testNullName() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		name = null;
		assertThrows(NullPointerException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testNullDosage() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		dosage = null;
		assertThrows(NullPointerException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testNullSteps() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		stepsToTake = null;
		assertThrows(NullPointerException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testEmpyName() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		name = "";
		assertThrows(IllegalArgumentException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testEmpyDosage() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		dosage = "";
		assertThrows(IllegalArgumentException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testEmpySteps() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		stepsToTake = "";
		assertThrows(IllegalArgumentException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testZeroRefills() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		refillsPerRenew = 0;
		RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
				noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		assertEquals(rx.getRefillsPerRenew(), 0);
	}

	@Test
	public void testNegativeRefills() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		refillsPerRenew = -1;
		assertThrows(IllegalArgumentException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

	@Test
	public void testZeroDoses() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		dayTimePairs.add(new DayTimePair(DayOfWeek.MONDAY, LocalTime.now()));
		dosesPerRefill = 0;
		assertThrows(IllegalArgumentException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}
	
	@Test
	public void testEmptyDayTimePairs() {
		
		ObservableList<DayTimePair> dayTimePairs = FXCollections.observableArrayList();
		assertThrows(IllegalArgumentException.class, () -> {
			RX rx = new RX(name, dosage, refillsPerRenew, dosesPerRefill, stepsToTake, noDrivingAfterTaking,
					noAlcoholAfterTaking, dayTimePairs, foodRequirements);
		});
	}

}