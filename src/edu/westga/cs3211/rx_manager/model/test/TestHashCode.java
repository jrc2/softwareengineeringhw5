package edu.westga.cs3211.rx_manager.model.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Objects;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;

class TestHashCode {

	@Test
	void testHashCode() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.FRIDAY, LocalTime.parse("11:00"));
		assertEquals(Objects.hash(DayOfWeek.FRIDAY, LocalTime.parse("11:00")), dayTimePair.hashCode());
	}

}
