package edu.westga.cs3211.rx_manager.model.test.FoodRequirements;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.FoodRequirements;

class TestToString {

	@Test
	void testTakeWithFood() {
		assertEquals("Take with food", FoodRequirements.TAKE_WITH_FOOD.toString());
	}
	
	@Test
	void testTakeWithoutFood() {
		assertEquals("Take without food", FoodRequirements.TAKE_WITHOUT_FOOD.toString());
	}
	
	@Test
	void testNoRequirement() {
		assertEquals("Take with or without food", FoodRequirements.NO_REQUIREMENT.toString());
	}

}
