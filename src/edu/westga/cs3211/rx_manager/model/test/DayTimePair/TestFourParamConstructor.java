package edu.westga.cs3211.rx_manager.model.test.DayTimePair;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.AmPm;
import edu.westga.cs3211.rx_manager.model.DayTimePair;

class TestFourParamConstructor {
	
	@Test
	void TestValidInfo() {
		DayTimePair dayTime = new DayTimePair(DayOfWeek.MONDAY, "10", "30", AmPm.PM);
		assertEquals(DayOfWeek.MONDAY, dayTime.getDay());
		assertEquals(LocalTime.parse("22:30"), dayTime.getTime());
		assertEquals("10:30 PM", dayTime.getFormattedTime());
	}

	@Test
	void testTooSmallHour() {
		assertThrows(IllegalArgumentException.class, () -> {
			new DayTimePair(DayOfWeek.SATURDAY, "0", "30", AmPm.AM);
		});
	}
	
	@Test
	void testTooLargeHour() {
		assertThrows(IllegalArgumentException.class, () -> {
			new DayTimePair(DayOfWeek.SATURDAY, "13", "30", AmPm.AM);
		});
	}
	
	@Test
	void testTooSmallMinutes() {
		assertThrows(IllegalArgumentException.class, () -> {
			new DayTimePair(DayOfWeek.SATURDAY, "2", "-2", AmPm.AM);
		});
	}
	
	@Test
	void testTooLargeMinutes() {
		assertThrows(IllegalArgumentException.class, () -> {
			new DayTimePair(DayOfWeek.SATURDAY, "2", "60", AmPm.AM);
		});
	}


}
