package edu.westga.cs3211.rx_manager.model.test.DayTimePair;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;

class TestEquals {

	@Test
	void testNotDayTimePair() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.MONDAY, LocalTime.parse("10:30"));
		assertFalse(dayTimePair.equals("not me"));
	}
	
	@Test
	void testIsSameDayTimePair() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.MONDAY, LocalTime.parse("10:30"));
		assertTrue(dayTimePair.equals(new DayTimePair(DayOfWeek.MONDAY, LocalTime.parse("10:30"))));
	}
	
	@Test
	void testSameDayDifferentTime() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.MONDAY, LocalTime.parse("10:30"));
		assertFalse(dayTimePair.equals(new DayTimePair(DayOfWeek.MONDAY, LocalTime.parse("22:30"))));
	}
	
	@Test
	void testDifferentDaySameTime() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.MONDAY, LocalTime.parse("10:30"));
		assertFalse(dayTimePair.equals(new DayTimePair(DayOfWeek.WEDNESDAY, LocalTime.parse("10:30"))));
	}

}
