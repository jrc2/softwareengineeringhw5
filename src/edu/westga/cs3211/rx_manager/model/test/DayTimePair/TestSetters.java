package edu.westga.cs3211.rx_manager.model.test.DayTimePair;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;

class TestSetters {

	@Test
	void testSetDay() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.MONDAY, LocalTime.now());
		dayTimePair.setDay(DayOfWeek.WEDNESDAY);
		assertEquals(DayOfWeek.WEDNESDAY, dayTimePair.getDay());
	}
	
	@Test
	void testSetTime() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.MONDAY, LocalTime.now());
		dayTimePair.setTime(LocalTime.parse("10:22"));
		assertEquals("10:22 AM", dayTimePair.getFormattedTime());
	}

}
