package edu.westga.cs3211.rx_manager.model.test.DayTimePair;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs3211.rx_manager.model.DayTimePair;

class TestToString {

	@Test
	void testToString() {
		DayTimePair dayTimePair = new DayTimePair(DayOfWeek.FRIDAY, LocalTime.parse("10:23"));
		assertEquals("FRIDAY 10:23 AM", dayTimePair.toString());
	}

}
