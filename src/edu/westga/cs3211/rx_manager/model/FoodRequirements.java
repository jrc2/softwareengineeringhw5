package edu.westga.cs3211.rx_manager.model;

/**
 * The possible food requirement options 
 * 
 * @author John Chittam
 *
 */
public enum FoodRequirements {
	TAKE_WITH_FOOD("Take with food"),
	TAKE_WITHOUT_FOOD("Take without food"),
	NO_REQUIREMENT("Take with or without food");
	
	private final String text;
	
	FoodRequirements(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
